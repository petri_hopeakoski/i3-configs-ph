#!/bin/bash

echo
echo "INSTALLING PACKAGES"
echo

PKGS=(

    'archlinux-keyring'		
    'sakura'			
    'firefox'
    'gtkhash'				
    'usbutils'				
    'modem-manager-gui'		
    'usb_modeswitch'		
    'nm-connection-editor'	
    'nitrogen'				
    'lxappearance'		
    'lightdm'			
    'lightdm-gtk-greeter'	
    'lightdm-gtk-greeter-settings'
    'curl'                  
    'bashtop'              
    'ufw'                   
    'htop'                  
    'inxi'                  
    'neofetch'              
    'unzip'                                   
    'ranger'				
    'xfburn'                
    'timeshift'				
    'geany'                 
    'micro'					
    'git'                   
    'meld'                 
    'torbrowser-launcher'   
    'webtorrent'	
    'galculator'        	
    'celluloid'            
    'gimp'                 
    'inkscape'             
    'shotwell'              
    'handbrake'				
    'gnome-disk-utility'	
    'gnome-boxes'			
    'xfce4-screenshooter'	
    'xfce4-terminal'		
    'simplescreenrecorder'	
    'rofi'
    'picom'					
    'xarchiver'
    'evince'
    'foliate'
    'qview'
    'yay'
    'dmenu'				

)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
    sudo pacman -Syyu
    sudo systemctl enable lightdm
    sudo systemctl enable ufw
done

echo
echo "Done!"
echo
